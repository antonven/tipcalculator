package tipcalc.anton.com.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int prog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final EditText billTxt1 = (EditText) findViewById(R.id.billTxt1);
        final EditText peopleTxt1 = (EditText) findViewById(R.id.peopleTxt1);
        final SeekBar tipPercentage = (SeekBar)findViewById(R.id.tipPercentage);
        final TextView tipDisplay = (TextView)findViewById(R.id.tipDisplay);
        final TextView percentDisplay = (TextView)findViewById(R.id.percentDisplay);


        assert billTxt1 != null;
        billTxt1.addTextChangedListener(new TextWatcher() {
            final TextView personDisplay = (TextView)findViewById(R.id.personDisplay);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(peopleTxt1.getText() == null){
                    tipDisplay.setText("Php 0.00");
                    personDisplay.setText("Php 0.00");
                }
                disp();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        assert peopleTxt1 != null;
        peopleTxt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disp();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        assert tipPercentage != null;
        tipPercentage.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                prog = tipPercentage.getProgress();
                percentDisplay.setText(prog + "%");
                disp();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    public void disp() {
        TextView tipDisplay = null;
        TextView personDisplay = null;
        try {
            final EditText billTxt = (EditText) findViewById(R.id.billTxt1);
            final EditText peopleTxt = (EditText) findViewById(R.id.peopleTxt1);
            personDisplay = (TextView) findViewById(R.id.personDisplay);
            tipDisplay = (TextView) findViewById(R.id.tipDisplay);

            float quo = 0.f, dv1, dv2, tippp;
            if (peopleTxt.getText() == null) {
                personDisplay.setText(quo + "");
                tipDisplay.setText(quo + "");
            } else {
                dv1 = Float.parseFloat(String.valueOf(billTxt.getText()));
                dv2 = Float.parseFloat(String.valueOf(peopleTxt.getText()));
                quo = dv1 / dv2;
                tippp = (dv1 / dv2) * ((float) prog / 100);
                tipDisplay.setText(tippp + "");
                personDisplay.setText(quo + tippp + "");
            }
        } catch (Exception e) {

            tipDisplay.setText("0.0");
            personDisplay.setText("0.0");
        }
    }

}
